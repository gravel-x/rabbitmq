<?php

namespace RabbitMq;

class MqBase extends Connect
{
    protected $exChangePrefix;
    protected $queuePrefix;

    public function __construct()
    {
        $this->exChangePrefix = config('rabbitmq.exChangeNamePrefix', env('APP_NAME', ''));
        $this->queuePrefix = config('rabbitmq.queueNamePrefix', env('APP_NAME', ''));
    }

    /**
     * exchange and queue name add prefix handle
     * @param string $prefix
     * @param string $name
     * @return string
     */
    protected function nameHandle(string $prefix, string $name): string
    {
        $str = $prefix . '.' . $name;
        return str_replace('..', '.', $str);
    }

    /**
     * get exchange name
     * @param string $exName
     * @return string
     */
    public function getExName(string $exName): string
    {
        return $this->nameHandle($this->exChangePrefix, $exName);
    }

    /**
     * get queue name
     * @param string $queueName
     * @return string
     */
    public function getQueueName(string $queueName): string
    {
        return $this->nameHandle($this->queuePrefix, $queueName);
    }

}