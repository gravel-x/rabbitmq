<?php

namespace RabbitMq;

class Producer extends MqBase
{
    /**
     * rabbitmq create producer  使用try 不影响主业务
     * @param string $exChangeName
     * @param array $msg
     * @param string $routingKey
     * @param bool $failedInDb 生产创建失败是否加入db 重试表
     * @return bool
     */
    public function create(string $exChangeName, array $msg, string $routingKey = '', bool $failedInDb = true): bool
    {
        try {

            $ex = $this->exChange();
            $ex->setName($this->getExName($exChangeName));
            $re = $ex->publish(json_encode($msg, JSON_UNESCAPED_UNICODE), $routingKey != '' ? $routingKey : null);
            if (!$re) {
                // log_error(Connect::PRODUCER . 'create FAILED', func_get_args());
            }

            return $re;
        } catch (\Exception $e) {
            // log_error(Connect::PRODUCER . 'create Exception' . $e->getMessage(), func_get_args());
            return false;
        }
    }
}