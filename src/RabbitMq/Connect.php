<?php

namespace RabbitMq;

class Connect
{
    protected $conn;
    protected $ch;
    protected $exCh;
    protected $qu;

    const PREFIX = 'rabbitmq';
    const PRODUCER = self::PREFIX . '-producer:';
    const CONSUMER = self::PREFIX . '-Consumer:';
    const AMQP_CONN = self::PREFIX . '-amqpConnect:';
    const EXCHANGE = self::PREFIX . '-exChange:';
    const QUEUE = self::PREFIX . '-queue:';

    /**
     * create amqp rabbitmq connect
     * @return \AMQPConnection
     * @throws \AMQPConnectionException
     */
    protected function connect(): \AMQPConnection
    {
        if ($this->conn instanceof \AMQPConnection) {
            return $this->conn;
        }
        $this->conn = new \AMQPConnection(config('rabbitmq.connect'));

        if (!$this->conn->connect()) {
            throw new \Exception('amqp connection failure');
        }

        if (!$this->conn->isConnected()) {
            throw new \Exception('amqp connection isConnected result: false');
        }
        return $this->conn;
    }

    /**
     * create amqp rabbitmq channel
     * @return \AMQPChannel
     * @throws \AMQPConnectionException
     */
    public function channel(): \AMQPChannel
    {
        if ($this->ch instanceof \AMQPChannel) {
            return $this->ch;
        }
        return $this->ch = new \AMQPChannel($this->connect());
    }

    /**
     * create amqp exchange
     * @param bool $reOpen
     * @return \AMQPExchange
     * @throws \AMQPConnectionException
     * @throws \AMQPExchangeException
     */
    public function exChange(bool $reOpen = true): \AMQPExchange
    {
        if ($reOpen || !($this->exCh instanceof \AMQPExchange)) {
            return $this->exCh = new \AMQPExchange($this->channel());
        }
        return $this->exCh;
    }

    /**
     * create amqp queue
     * @param bool $reOpen
     * @return \AMQPQueue
     * @throws \AMQPConnectionException
     * @throws \AMQPQueueException
     */
    public function queue(bool $reOpen = true): \AMQPQueue
    {
        if ($reOpen || !($this->qu instanceof \AMQPQueue)) {
            return $this->qu = new \AMQPQueue($this->channel());
        }
        return $this->qu;
    }

    /**
     * close channel and amqp.connect
     */
    public function __destruct()
    {
        if ($this->ch instanceof \AMQPChannel) {
            $this->ch->close();
        }
        if ($this->conn instanceof \AMQPConnection) {
            $this->conn->disconnect();
        }
    }
}