<?php

namespace RabbitMq;

class ExChangeQueue extends MqBase
{

    public $initExError = '';
    public $initQueueError = '';

    /**
     * create exchange and bind routingKey
     * @param string $type
     * @param string $exchangeName
     * @param string $routingKey
     * @return bool
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPExchangeException
     */
    public function createExchangeBind(string $type, string $exchangeName, string $routingKey = ''): bool
    {
        $exchangeName = $this->getExName($exchangeName);
        $ex = $this->exChange();
        $ex->setName($exchangeName);
        $ex->setType($type);
        return $ex->declareExchange();
    }

    /**
     * create queue and bind routingKey
     * @param string $exChangeName
     * @param string $queueName
     * @param string $routingKey
     * @param int $flag
     * @return bool
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPQueueException
     */
    public function createQueueBind(string $exChangeName, string $queueName, string $routingKey = '', int $flag = AMQP_DURABLE): bool
    {
        $queue = $this->queue();
        $queue->setName($this->getQueueName($queueName));
        $queue->setFlags($flag);
        $queue->declareQueue();
        return $queue->bind($this->getExName($exChangeName), $routingKey != '' ? $routingKey : null);
    }


    /**
     * rabbitmq config init  exchange and queues into mq
     * @param array $config
     * @return bool
     */
    public function configInitToMq(array $config = []): bool
    {
        try {
            if (empty($config)) {
                $config = config('rabbitmq.relation');
            }
            if (empty($config)) {
                throw new \Exception('rabbitmq config key:relation is empty');
            }

            $re = true;
            foreach ($config as $item) {
                $re = $this->initExchanges($item);
                if (!$re) {
                    break;
                }
            }
            return $re;
        } catch (\Exception $e) {
            log_error(parent::PREFIX . '-configInitToMq  Exception' . $e->getMessage());
            return false;
        }
    }

    /**
     * config init exchanges
     * @param array $item
     * @return bool
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPExchangeException
     */
    private function initExchanges(array $item): bool
    {

        if (empty($item)) return true;
        $itemRe = $this->createExchangeBind($item['exChangeType'], $item['exChangeName'], $item['routingKey'] ?? '');
        if (!$itemRe) {
            throw new \Exception('init-mq:createExchangeBind ' . $item['exChangeType'] . 'exChangeName' . $item['exChangeName'] . ' init failed');
        }

        $queueRe = $this->initExchangeItemQueues($item['queues'], $item['exChangeName']);
        if (!$queueRe) {
            throw new \Exception('init-mq:initExchangeItemQueues ' . $item['exChangeType'] . 'exChangeName' . $item['exChangeName'] . ' init failed');
        }
        return $queueRe;
    }

    /**
     * config init queues
     * @param array $exItemQueues
     * @param string $exChangeName
     * @return bool
     */
    private function initExchangeItemQueues(array $exItemQueues, string $exChangeName): bool
    {
        try {

            if (empty($exItemQueues)) return true;
            $re = false;
            foreach ($exItemQueues as $item) {
                $re = $this->createQueueBind($exChangeName, $item['queueName'], $item['routingKey'], $item['queueFlag']);
                if (!$re) {
                    throw new \Exception('init-mq:initExchangeItemQueues.key:exChangeName:' . $exChangeName . ' queues:' . $item['queueName'] . ' init failed');
                }
            }
            return $re;
        } catch (\Exception $e) {
            log_error(parent::QUEUE . ' “initExchangeItemQueues” Exception' . $e->getMessage());
            $this->initQueueError = $e->getMessage();
            return false;
        }
    }
}