<?php

namespace RabbitMq;

class Consumer extends MqBase
{

    // 需要监听的队列名称
    protected $queueName;
    // 消费逻辑class::class
    protected $class;

    public function __construct(string $queueName, string $class)
    {
        $this->queueName = $queueName;
        $this->class = $class;
        parent::__construct();
    }

    /**
     * mq消费
     * @param bool $return 是否仅处理一次
     * @param bool $ack 是否ack 确认
     * @return void
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     * @throws \AMQPEnvelopeException
     * @throws \AMQPQueueException
     */
    public function handle(bool $return = false, bool $ack = true): void
    {
        // 实例消费逻辑class
        $instance = new $this->class;
        $class = $this->class;

        // 初始化消费队列
        $queue = $this->setConsumerQueue();
        // 执行消费新起线程
        $queue->consume(function (\AMQPEnvelope $envelope, \AMQPQueue $checkQueue) use ($instance, $return, $ack, $class)
        {
            // queue 消息体 json
            $body = json_decode($envelope->getBody(), true);

            // 消息标记
            $deliveryTag = $envelope->getDeliveryTag();
            // 执行实例消费逻辑class::handle
            $re = $instance::{'handle'}($body);
            // 根据消费标记确认消费
            if ($re->status) {
                job_info(Connect::CONSUMER . '"' . $class . '" action:"handle" SUCCESS', $body);
                if ($ack) $checkQueue->ack($deliveryTag);
            } else {
                job_err(Connect::CONSUMER . '"' . $class . '" action:"handle" FAILED', $body);
            }
            // 线程权限移交给闭包外部
            if ($return) {
                return false;
            }
        });
    }

    /**
     * 设置消费队列
     * @return \AMQPQueue
     * @throws \AMQPConnectionException
     * @throws \AMQPQueueException
     */
    private function setConsumerQueue(): \AMQPQueue
    {
        $queue = $this->queue();
        $queue->setName($this->getQueueName($this->queueName));
        return $queue;
    }
}